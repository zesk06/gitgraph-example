serve:
	@yarn serve
init:
	@yarn install

build: ##  build the project to public folder
	@rm -fr public
	@cp -vfR -L src public
